const express = require("express");
const app = express();

app.use(express.json());

const user = [
  {
    id: 1,
    name: "user1",
  },
  {
    id: 2,
    name: "user2",
  },
  {
    id: 3,
    name: "user3",
  },
  {
    id: 4,
    name: "user4",
  },
  {
    id: 5,
    name: "user5",
  },
  {
    id: 6,
    name: "user6",
  },
  {
    id: 7,
    name: "user7",
  },
  {
    id: 8,
    name: "user8",
  },
  {
    id: 9,
    name: "user9",
  },
  {
    id: 10,
    name: "user10",
  },
];

app.post("/demo", (req, res) => {
  res.send("Hello World");
});

app.get("/pagination", (req, res) => {
  const page = parseInt(req.query.page);
  const limit = parseInt(req.query.limit);
  const startindex = (page - 1) * limit;
  const endindex = page * limit;

  const result = {};

  if (endindex < user.length) {
    result.next = {
      page: page + 1,
      limit: limit,
    };
  }
  if (startindex > 0) {
    result.previous = {
      page: page - 1,
      limit: limit,
    };
  }

  result.results = user.slice(startindex, endindex);
  res.json(result);
});

app.listen(4000, () => {
  console.log("Server is opened");
});
